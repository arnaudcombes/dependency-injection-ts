import { forEach } from "../utils/forEach";
import { TypeGuards } from "../utils/TypeGuards";

export interface INode<T> {
    data: T;
    incoming: { [key: string]: INode<T> };
    outgoing: { [key: string]: INode<T> };
}

function newNode<T>(data: T): INode<T> {
    return {
        data: data,
        incoming: Object.create(null),
        outgoing: Object.create(null),
    };
}

export class Graph<T> {
    private _nodes: { [key: string]: INode<T> } = Object.create(null);
    constructor(private _hasFnKey: (element: T) => string) {}
    roots(): INode<T>[] {
        const ret: INode<T>[] = [];
        forEach(this._nodes, entry => {
            if (TypeGuards.isEmptyObject(entry.value.outgoing)) {
                ret.push(entry.value);
            }
        });
        return ret;
    }

    insertEdge(from: T, to: T): void {
        const fromNode = this.lookupOrInsertNode(from);
        const toNode = this.lookupOrInsertNode(to);
        fromNode.outgoing[this._hasFnKey(to)] = toNode;
        toNode.incoming[this._hasFnKey(from)] = fromNode;
    }

    removeNode(data: T): void {
        const key = this._hasFnKey(data);
        delete this._nodes[key];
        forEach(this._nodes, entry => {
            delete entry.value.outgoing[key];
            delete entry.value.incoming[key];
        });
    }

    lookupOrInsertNode(data: T): INode<T> {
        const key = this._hasFnKey(data);
        let node = this._nodes[key];

        if (!node) {
            node = newNode(data);
            this._nodes[key] = node;
        }

        return node;
    }

    lookup(data: T): INode<T> {
        return this._nodes[this._hasFnKey(data)];
    }

    isEmpty(): boolean {
        for (const _key in this._nodes) {
            return false;
        }
        return true;
    }

    toString(): string {
        const data: string[] = [];
        const keys = Object.keys;
        forEach(this._nodes, entry => {
            data.push(
                `${entry.key}: (incoming)[${keys(entry.value.incoming).join(
                    ", "
                )}], (outgoing)[${keys(entry.value.outgoing).join(",")}]`
            );
        });
        return data.join("\n");
    }
}
