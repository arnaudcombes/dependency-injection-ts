import { Graph } from "./Graph";

describe("Graph", () => {
    describe("IsEmpty", () => {
        it("should return true if the graph is empty", () => {
            const graph = new Graph<string>(s => s);
            expect(graph.isEmpty()).toBe(true);
        });
        it("should return false if the graph is empty", () => {
            const graph = new Graph<string>(s => s);
            graph.lookupOrInsertNode("nodeA");
            expect(graph.isEmpty()).toBe(false);
        });
    });

    it("is possible to lookup nodes that don't exist", () => {
        const graph = new Graph<string>(s => s);
        const node = graph.lookup("node");
        expect(node).toBe(undefined);
    });

    it("node", () => {
        const graph = new Graph<string>(s => s);
        const nodeA = graph.lookupOrInsertNode("nodeA");
        const nodeB = graph.lookupOrInsertNode("nodeB");
        expect(nodeA.data).toBe("nodeA");
        expect(nodeB.data).toBe("nodeB");
    });

    it("root", () => {
        const graph = new Graph<string>(s => s);
        graph.insertEdge("Node A", "Node B");
        let roots = graph.roots();
        expect(roots).toHaveLength(1);
        graph.insertEdge("Node B", "Node A");
        roots = graph.roots();
        expect(roots).toHaveLength(0);
    });

    test("root complex", function() {
        const graph = new Graph<string>(s => s);

        graph.insertEdge("1", "2");
        graph.insertEdge("1", "3");
        graph.insertEdge("3", "4");

        const roots = graph.roots();

        expect(roots).toHaveLength(2);
    });

    it("should works with circular dependency ", () => {
        const graph = new Graph<string>(s => s);

        const nodeA = "Node A";
        const nodeB = "Node B";

        graph.lookupOrInsertNode(nodeA);
        graph.lookupOrInsertNode(nodeB);

        graph.insertEdge(nodeA, nodeB);
        graph.insertEdge(nodeB, nodeA);

        expect(graph.lookup(nodeA).incoming[nodeB].data).toBe(nodeB);
        expect(graph.lookup(nodeB).incoming[nodeA].data).toBe(nodeA);
    });
});
