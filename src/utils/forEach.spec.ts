import { forEach, INumberDictionary } from "./forEach";

describe("forEach", () => {
    const plainObj: INumberDictionary<string> = {
        0: "first value",
        1: "second value",
    };

    it("should loops through keys/value", () => {
        const arr: { key: string; value: string }[] = [];

        forEach(plainObj, ({ key, value }) => {
            arr.push({ key, value });
        });

        expect(arr[0].key).toBe("0");
        expect(arr[0].value).toBe("first value");
        expect(arr[1].key).toBe("1");
        expect(arr[1].value).toBe("second value");
    });

    it("should stop looping through keys/value if the callback returns false", () => {
        const arr: { key: string; value: string }[] = [];

        forEach<string>(plainObj, ({ key, value }) => {
            const keyNumber = Number(key);
            if (keyNumber >= 0) {
                arr.push({ key, value });
            }
            return false;
        });

        expect(arr[0].key).toBe("0");
        expect(arr[0].value).toBe("first value");
        expect(arr[1]).toBe(undefined);
        expect(arr[1]).toBe(undefined);
    });
});
