export interface IStringDictionary<V> {
    [name: string]: V;
}

/**
 * An interface for a JavaScript object that
 * acts a dictionary. The keys are numbers.
 */
export interface INumberDictionary<V> {
    [idx: number]: V;
}

export function forEach<T>(
    dictionary: IStringDictionary<T> | INumberDictionary<T>,
    callback: (entry: { key: string; value: T }) => boolean | void
): void {
    for (const key in dictionary) {
        if (Object.hasOwnProperty.call(dictionary, key)) {
            const result = callback({
                key: key,
                value: (dictionary as any)[key],
            });
            if (result === false) {
                return;
            }
        }
    }
}
