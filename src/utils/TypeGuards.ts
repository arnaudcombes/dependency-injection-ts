/* eslint-disable @typescript-eslint/no-explicit-any */

const _typeof = {
    number: "number",
    string: "string",
    undefined: "undefined",
    object: "object",
    function: "function",
};

function isObject(obj: any): obj is Record<string, any> {
    return (
        typeof obj === _typeof.object &&
        obj !== null &&
        !Array.isArray(obj) &&
        !(obj instanceof RegExp) &&
        !(obj instanceof Date)
    );
}

const hasOwnProperty = Object.hasOwnProperty;

function isEmptyObject(obj: any): obj is any {
    if (!isObject(obj)) {
        return false;
    }

    for (const key in obj) {
        if (hasOwnProperty.call(obj, key)) {
            return false;
        }
    }

    return true;
}

export const TypeGuards = {
    /**
     * @returns true whether the provided parameter is object, and not:
     * - null / undefined
     * - array
     * - date
     * - regex
     */
    isObject,
    /**
     * @returns true whether the provided parameter is an empty object or not.
     */
    isEmptyObject,
};
