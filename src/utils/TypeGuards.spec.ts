import { TypeGuards } from "./TypeGuards";

describe("TypeGuards", () => {
    describe("isObject", () => {
        it("should return false if the object is an array.", () => {
            class ArrayMaker extends Array {}
            const myObjectFromClass = new ArrayMaker();
            const plainArray = [];
            expect(TypeGuards.isObject(myObjectFromClass)).toBe(false);
            expect(TypeGuards.isObject(plainArray)).toBe(false);
        });

        it("should return false if the object is Date.", () => {
            const date = new Date();
            expect(TypeGuards.isObject(date)).toBe(false);
        });

        it("should return false if the object is null or undefined.", () => {
            expect(TypeGuards.isObject(undefined)).toBe(false);
            expect(TypeGuards.isObject(null)).toBe(false);
        });

        it("should return false if the object is regex.", () => {
            const regex = /^[1-9]\d{0,2}$/g;
            const regexWithConstructor = new RegExp("^[1-9]d{0,2}$");
            expect(TypeGuards.isObject(regex)).toBe(false);
            expect(TypeGuards.isObject(regexWithConstructor)).toBe(false);
        });

        it("should return false if the object is function/class.", () => {
            expect(TypeGuards.isObject(new Function())).toBe(false);
            expect(TypeGuards.isObject(class {})).toBe(false);
        });

        it("should return true if the object is an object.", () => {
            class ObjectMaker {}
            const myObjectFromClass = new ObjectMaker();
            const plainObj = { message: "Hello world" };
            expect(TypeGuards.isObject(myObjectFromClass)).toBe(true);
            expect(TypeGuards.isObject(plainObj)).toBe(true);
        });
    });
    describe("isEmptyObject", () => {
        it("should return true if the object is empty", () => {
            expect(TypeGuards.isEmptyObject({})).toBe(true);
        });
        it("should return false if the object is null or undefined", () => {
            expect(TypeGuards.isEmptyObject(null)).toBe(false);
            expect(TypeGuards.isEmptyObject(undefined)).toBe(false);
        });
    });
});
